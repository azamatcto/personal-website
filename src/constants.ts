import Project1 from '@/assets/images/project-1.jpg'
import Project2 from '@/assets/images/project-2.jpg'
import Project3 from '@/assets/images/project-3.jpg'

export const links = [
  {
    key: 'home',
    label: 'Home',
    url: 'home'
  },
  {
    key: 'about',
    label: 'About Me',
    url: 'about'
  },
  {
    key: 'projects',
    label: 'Projects',
    url: 'projects'
  }
]

export const projects = [
  {
    key: 'restaurant',
    img: Project1,
    demoUrl: '',
    subtitle: 'Website',
    title: 'Restaurant Website',
    description:
      "Project that you carry out in the design and structure of the layout, showing the design at the client's request.",
    repoUrl: ''
  },
  {
    key: 'yoga',
    img: Project2,
    demoUrl: '',
    subtitle: 'App',
    title: 'Yoga App',
    description:
      "Project that you carry out in the design and structure of the layout, showing the design at the client's request.",
    repoUrl: ''
  },
  {
    key: 'fast-food',
    img: Project3,
    demoUrl: '',
    subtitle: 'App',
    title: 'Fast Food App',
    description:
      "Project that you carry out in the design and structure of the layout, showing the design at the client's request.",
    repoUrl: ''
  },
  {
    key: 'coffee',
    img: Project1,
    demoUrl: '',
    subtitle: 'Website',
    title: 'Coffee Delivery Website',
    description:
      "Project that you carry out in the design and structure of the layout, showing the design at the client's request.",
    repoUrl: ''
  },
  {
    key: 'barbershop',
    img: Project2,
    demoUrl: '',
    subtitle: 'Website',
    title: 'Barbershop Website',
    description:
      "Project that you carry out in the design and structure of the layout, showing the design at the client's request.",
    repoUrl: ''
  }
]
