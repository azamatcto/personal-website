import AboutImage from '@/assets/images/about-profile.jpg'
import RandomLines from '@/assets/images/random-lines.svg'
import { Button } from '@/components/ui/button'
import { GithubLogo, PaperPlaneTilt } from '@phosphor-icons/react'

function About() {
  return (
    <section className="about section" id="about">
      <div className="container">
        <div className="about__row">
          <h2 className="section__title-1">
            <span>About Me.</span>
          </h2>

          <div className="about__profile">
            <div className="about__image">
              <img className="about__img" src={AboutImage} alt="About image" />
              <div className="about__shadow" />

              <div className="about__geometric-box geometric-box" />
              <img
                className="about__random-lines"
                src={RandomLines}
                alt="Random lines svg image"
              />
              <div className="about__box" />
            </div>
          </div>

          <div className="about__info">
            <p className="about__description">
              Passionate about creating <b>Web Pages</b> with
              <b> UI/UX User Interface</b>, I have 1+ years of experience and
              many clients are happy with the projects carried out.
            </p>

            <h2 className="about__title">My skills</h2>

            <div className="about__skills">
              <ul className="about__list">
                <h4>Basics</h4>
                <li className="about__item">HTML5</li>
                <li className="about__item">CSS3</li>
                <li className="about__item">BEM</li>
                <li className="about__item">Figma</li>
                <li className="about__item">Responsive Web Design (RWD)</li>
                <li className="about__item">Cross browser</li>
              </ul>

              <ul className="about__list">
                <h4>Advanced</h4>
                <li className="about__item">Git & GitHub</li>
                <li className="about__item">JavaScript (ES5/ES6+)</li>
                <li className="about__item">React</li>
                <li className="about__item">TailwindCSS, shadecn/ui</li>
                <li className="about__item">
                  Preprocessors - SASS, LESS, PostCSS
                </li>
                <li className="about__item">Redux</li>
                <li className="about__item">Typescript</li>
                <li className="about__item">Rest API, Axios</li>
                <li className="about__item">Principles - SOLID, DRY, KISS</li>
              </ul>
            </div>

            <div className="about__buttons">
              <Button asChild>
                <a href="#contact">
                  <PaperPlaneTilt size={20} />
                  Contact Me
                </a>
              </Button>

              <Button size="icon" variant="outline" asChild>
                <a href="https://github.com/azamat-cto" target="_blank">
                  <GithubLogo size={20} />
                </a>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default About
