import { Button } from '@/components/ui/button'
import {
  InstagramLogo,
  LinkedinLogo,
  PaperPlaneTilt,
  TelegramLogo
} from '@phosphor-icons/react'
import CurvedArrow from '@/assets/images/curved-arrow.svg'

function Contact() {
  return (
    <section className="contact section" id="contact">
      <div className="container">
        <div className="contact__row">
          <div className="contact__data">
            <h2 className="section__title-2">
              <span>Contact Me.</span>
            </h2>

            <p className="contact__description-1">
              I will read all emails. Send me any message you want and I'll get
              back to you.
            </p>

            <p className="contact__description-2">
              I need your <b>Name</b> and <b>Email Address</b>, but you won't
              receive anything other than your reply.
            </p>

            <div className="contact__geometric-box geometric-box" />
          </div>

          <div className="contact__mail">
            <h2 className="contact__title">Send Me A Message</h2>

            <form className="contact__form">
              <div className="contact__form-group">
                <div className="contact__form-box">
                  <input
                    className="contact__form-input"
                    type="text"
                    id="firstname"
                    name="firstname"
                    placeholder="First Name"
                    required
                  />
                  <label className="contact__form-label" htmlFor="firstname">
                    First Name
                  </label>
                </div>

                <div className="contact__form-box">
                  <input
                    className="contact__form-input"
                    type="email"
                    id="email"
                    name="email"
                    placeholder="Email Address"
                    required
                  />
                  <label className="contact__form-label" htmlFor="email">
                    Email Address
                  </label>
                </div>
              </div>

              <div className="contact__form-box">
                <input
                  className="contact__form-input"
                  type="text"
                  id="subject"
                  name="subject"
                  placeholder="Subject"
                  required
                />
                <label className="contact__form-label" htmlFor="subject">
                  Subject
                </label>
              </div>

              <div className="contact__form-box contact__form-area">
                <textarea
                  className="contact__form-input"
                  name="message"
                  id="message"
                  placeholder="Message"
                  required
                ></textarea>
                <label className="contact__form-label" htmlFor="message">
                  Message
                </label>
              </div>

              <Button
                className="contact__form-button hover:bg-primary"
                variant="primary"
              >
                <PaperPlaneTilt size={24} /> Send Message
              </Button>
            </form>
          </div>

          <div className="contact__social">
            <img
              className="contact__social-arrow"
              src={CurvedArrow}
              alt="Curved arrow icon svg"
            />

            <div className="contact__social-data">
              <div>
                <p className="contact__social-description-1">
                  Does not send emails
                </p>

                <p className="contact__social-description-2">
                  Write me on my social networks
                </p>
              </div>

              <div className="contact__social-links">
                <a
                  className="contact__social-link"
                  href="https://t.me/azamatcto"
                  target="_blank"
                >
                  <TelegramLogo size={24} />
                </a>

                <a
                  className="contact__social-link"
                  href="https://www.instagram.com/azamat.cto"
                  target="_blank"
                >
                  <InstagramLogo size={24} />
                </a>

                <a
                  className="contact__social-link"
                  href="https://www.linkedin.com/in/azamat-cto/"
                  target="_blank"
                >
                  <LinkedinLogo size={24} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Contact
