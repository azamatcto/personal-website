import { Button } from '@/components/ui/button'
import { projects } from '@/constants'
import { ArrowUpRight, GithubLogo, GlobeSimple } from '@phosphor-icons/react'

function Projects() {
  return (
    <section className="projects section" id="projects">
      <div className="container">
        <h2 className="section__title-1">
          <span>Projects.</span>
        </h2>
        <div className="projects__row">
          {projects.map((project) => (
            <article className="projects__card" key={project.key}>
              <div className="projects__image">
                <img
                  className="projects__img"
                  src={project.img}
                  alt={project.title}
                />

                <Button className="projects__btn" asChild>
                  <a href={project.demoUrl} target="_blank">
                    <ArrowUpRight size={24} />
                  </a>
                </Button>
              </div>

              <div className="projects__content">
                <h3 className="projects__subtitle">{project.subtitle}</h3>
                <h2 className="projects__title">{project.title}</h2>
                <p className="projects__description">{project.description}</p>
              </div>

              <div className="projects__buttons">
                <a
                  className="projects__link"
                  target="_blank"
                  href={project.repoUrl}
                >
                  <GithubLogo size={24} />
                  View
                </a>
                <a
                  className="projects__link"
                  target="_blank"
                  href={project.demoUrl}
                >
                  <GlobeSimple size={24} />
                  View
                </a>
              </div>
            </article>
          ))}
        </div>
      </div>
    </section>
  )
}

export default Projects
