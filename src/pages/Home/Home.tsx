import HomeImage from '@/assets/images/home-profile.jpg'
import CurvedArrow from '@/assets/images/curved-arrow.svg'
import RandomLines from '@/assets/images/random-lines.svg'
import {
  CaretDown,
  GithubLogo,
  InstagramLogo,
  TelegramLogo
} from '@phosphor-icons/react'
import { Link } from 'react-scroll'

function Home() {
  return (
    <section className="home section" id="home">
      <div className="container">
        <div className="home__row">
          <h1 className="home__name">Azamat.</h1>

          <div className="home__profile">
            <div className="home__image">
              <img className="home__img" src={HomeImage} alt="Profile image" />
              <div className="home__shadow" />

              <img
                className="home__curved-arrow"
                src={CurvedArrow}
                alt="Curved arrow svg image"
              />
              <img
                className="home__random-lines"
                src={RandomLines}
                alt="Random lines svg image"
              />
              <div className="home__geometric-box geometric-box" />
            </div>

            <div className="home__socials">
              <a
                className="home__socials-link"
                href="https://github.com/azamat-cto"
                target="_blank"
              >
                <GithubLogo />
              </a>
              <a
                className="home__socials-link"
                href="https://t.me/azamatcto"
                target="_blank"
              >
                <TelegramLogo />
              </a>
              <a
                className="home__socials-link"
                href="https://www.instagram.com/azamat.cto/"
                target="_blank"
              >
                <InstagramLogo />
              </a>
            </div>
          </div>

          <div className="home__info">
            <p className="home__description">
              <b>Frontend Developer</b>, with knowledge in web development and
              design, I offer the best projects resulting in quality work.
            </p>

            <Link
              className="home__scroll-down"
              href=""
              activeClass="active"
              spy={true}
              to="projects"
              offset={-4.5}
            >
              <span className="home__scroll-down-box">
                <CaretDown />
              </span>
              <span className="home__scroll-down-text">Scroll Down</span>
            </Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Home
