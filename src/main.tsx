import { createRoot } from 'react-dom/client'
import App from './App'
import 'unfonts.css'
import './styles/index.scss'
import { ThemeProvider } from 'next-themes'

const container = document.getElementById('root')!
const root = createRoot(container)

root.render(
  <ThemeProvider
    attribute="class"
    defaultTheme="system"
    enableSystem
  >
    <App />
  </ThemeProvider>
)
