import { Link } from 'react-scroll'
import { Sheet, SheetContent } from '@/components/ui/sheet'
import { Button } from '@/components/ui/button'
import { CloseMobileMenu } from '@/components/CloseMobileMenu'
import { links } from '@/constants'

interface MobileMenuProps {
  isOpen: boolean
  toggleMobileMenu: (isOpen: boolean) => () => void
}

function MobileMenu(props: MobileMenuProps) {
  const { isOpen, toggleMobileMenu } = props

  return (
    <Sheet open={isOpen} onOpenChange={toggleMobileMenu(false)}>
      <SheetContent side="top">
        <div className="mobile-menu">
          <span className="mobile-menu__title">Menu</span>

          <h3 className="mobile-menu__name">Azamat</h3>

          <nav className="mobile-menu__nav">
            <ul className="mobile-menu__list">
              {links.map((link) => (
                <li className="mobile-menu__item" key={link.key}>
                  <Link
                    className="mobile-menu__link"
                    href=""
                    activeClass="active"
                    to={link.url}
                    spy={true}
                    offset={-4}
                    onClick={toggleMobileMenu(false)}
                  >
                    {link.label}
                  </Link>
                </li>
              ))}
            </ul>

            <Button asChild>
              <Link
                className="mt-10"
                href=""
                activeClass="active"
                to="contact"
                spy={true}
                offset={-4}
                onClick={toggleMobileMenu(false)}
              >
                Contact Me
              </Link>
            </Button>
          </nav>

          <CloseMobileMenu toggleMobileMenu={toggleMobileMenu} />
        </div>
      </SheetContent>
    </Sheet>
  )
}

export default MobileMenu
