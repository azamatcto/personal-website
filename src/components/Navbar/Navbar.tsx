import { links } from '@/constants'
import { Link } from 'react-scroll'

function Navbar() {
  return (
    <nav className="navbar">
      <ul className="navbar__list">
        {links.map((link) => {
          return (
            <li className="navbar__item" key={link.key}>
              <Link
                className="navbar__link"
                href=""
                activeClass="active"
                spy={true}
                to={link.url}
                offset={-4.5}
              >
                {link.label}
              </Link>
            </li>
          )
        })}
      </ul>
    </nav>
  )
}

export default Navbar
