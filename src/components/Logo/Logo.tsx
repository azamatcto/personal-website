import { Link } from 'react-scroll'

function Logo() {
  return (
    <div className="logo">
      <Link
        className="logo__link"
        href=""
        activeClass="active"
        to="home"
        spy={true}
        offset={-5}
      >
        <span className="logo__letter">A</span>
        <span className="logo__name">Azamat.</span>
      </Link>
    </div>
  )
}

export default Logo
