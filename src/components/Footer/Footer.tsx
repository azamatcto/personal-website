import { links } from '@/constants'
import { Link } from 'react-scroll'

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__row">
          <ul className="footer__links">
            {links.map((link) => (
              <li className="footer__item" key={link.key}>
                <Link
                  className="footer__link"
                  href=""
                  activeClass="active"
                  to={link.url}
                  spy={true}
                  offset={-4.5}
                >
                  {link.label}
                </Link>
              </li>
            ))}
          </ul>

          <span className="footer__copy">
            &#169; All Rights Reserved By
            <b> Azamat.</b>
          </span>
        </div>
      </div>
    </footer>
  )
}

export default Footer
