import { Button } from '../ui/button'
import { List } from '@phosphor-icons/react'

interface OpenMobileMenuProps {
  toggleMobileMenu: (isOpen: boolean) => () => void
}

function OpenMobileMenu(props: OpenMobileMenuProps) {
  const { toggleMobileMenu } = props

  return (
    <Button
      className="open-mobile-menu"
      size="icon"
      variant="ghost"
      onClick={toggleMobileMenu(true)}
    >
      <List size={24} />
    </Button>
  )
}

export default OpenMobileMenu
