import { useEffect, useState } from 'react'
import { Logo } from '@/components/Logo'
import { Navbar } from '../Navbar'
import { ToggleTheme } from '../ToggleTheme'
import { OpenMobileMenu } from '../OpenMobileMenu'
import { MobileMenu } from '../MobileMenu'
import { Button } from '../ui/button'
import { Link } from 'react-scroll'

function Header() {
  const [isOpen, setIsOpen] = useState<boolean>(false)
  const [scrollHeader, setScrollHeader] = useState<boolean>(false)

  const toggleMobileMenu = (isOpen: boolean) => () => {
    setIsOpen(isOpen)
  }

  const changeHeaderBgOnScroll = () => {
    if (window.scrollY >= 50) {
      setScrollHeader(true)
    } else {
      setScrollHeader(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', changeHeaderBgOnScroll)

    return () => {
      window.removeEventListener('scroll', changeHeaderBgOnScroll)
    }
  }, [])

  return (
    <>
      <header className={`${scrollHeader ? 'header scroll' : 'header'}`}>
        <div className="container">
          <div className="header__row">
            <Logo />

            <Navbar />

            <div className="header__buttons">
              <Button asChild>
                <Link
                  className="contact-btn"
                  href=""
                  activeClass="active"
                  to="contact"
                  spy={true}
                  offset={-4}
                >
                  Contact Me
                </Link>
              </Button>

              <ToggleTheme />

              <OpenMobileMenu toggleMobileMenu={toggleMobileMenu} />
            </div>
          </div>
        </div>
      </header>

      <MobileMenu isOpen={isOpen} toggleMobileMenu={toggleMobileMenu} />
    </>
  )
}

export default Header
