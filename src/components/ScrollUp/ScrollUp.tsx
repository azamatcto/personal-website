import { useState, useEffect } from 'react'
import { CaretUp } from '@phosphor-icons/react'
import { Link } from 'react-scroll'

function ScrollUp() {
  const [scrollUp, setScrollUp] = useState<boolean>(false)

  const handleScrollUp = () => {
    window.addEventListener('scroll', () => {
      if (window.scrollY >= 350) {
        setScrollUp(true)
      } else {
        setScrollUp(false)
      }
    })
  }

  useEffect(() => {
    handleScrollUp()
    return () => {
      window.removeEventListener('scroll', handleScrollUp)
    }
  }, [])

  return (
    <Link
      className={scrollUp ? 'scroll-up scrolled' : 'scroll-up'}
      href=""
      activeClass="active"
      to="home"
      spy={true}
      offset={-4.5}
    >
      <CaretUp size={24} />
    </Link>
  )
}

export default ScrollUp
