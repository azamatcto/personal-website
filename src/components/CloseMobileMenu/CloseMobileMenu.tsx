import { X } from '@phosphor-icons/react'
import { Button } from '../ui/button'

interface CloseMobileMenuProps {
  toggleMobileMenu: (isOpen: boolean) => () => void
}

function CloseMobileMenu(props: CloseMobileMenuProps) {
  const { toggleMobileMenu } = props

  return (
    <Button
      size="icon"
      variant="ghost"
      className="absolute top-4 right-4 text-white"
      onClick={toggleMobileMenu(false)}
    >
      <X size={24} />
    </Button>
  )
}

export default CloseMobileMenu
