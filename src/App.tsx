import { Header } from './components/Header'
import { Home } from '@/pages/Home'
import { About } from '@/pages/About'
import { Services } from '@/pages/Services'
import { Projects } from '@/pages/Projects'
import { Contact } from '@/pages/Contact'
import { Footer } from '@/components/Footer'
import { ScrollUp } from './components/ScrollUp'

function App() {
  return (
    <>
      <Header />
      <main className="main">
        <Home />
        <About />
        <Services />
        <Projects />
        <Contact />
      </main>
      <Footer />
      <ScrollUp />
    </>
  )
}

export default App
